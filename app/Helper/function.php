<?php
// trong app tạo folder helper
// tạo file function trong folder helper, viết các hàm xử lí tại đây


// vào file composer.json thêm đường dẫn vào phần autoload
// "autoload": { 

// 	.....
// 	"files": [ 
// 	"app/helper/function.php" 
// 	]  

// 	}, 


// chạy lệnh  composer dump-autoload, trong file (vendor/composer/autoload_file.php tự sinh ra thêm 1 đường dẫn (ktra): '4ef25952a3d3f7c399fe269e8e36b783' => $baseDir . '/app/helper/function.php',  )

// => thường dùng để viết API

function ShowErrors($errors, $nameinput){

	if($errors->has($nameinput))
	{
        echo '<div class="alert alert-danger" ><strong>';

        echo $errors->first($nameinput);
            
        echo "</strong></div>";
                                        
    }
}

function getCategory($danhMuc, $idCha, $chuoiTab){
	foreach($danhMuc as $banGhi){
		if($banGhi['parent']==$idCha){
			echo '<option value = "'.$banGhi['id'].'">'.$chuoiTab.$banGhi['name'].'</option>';  			
			getCategory($danhMuc, $banGhi['id'], $chuoiTab.'---|');
		}
	}	
}

function getCategoryEdit($danhMuc, $idCha, $chuoiTab, $idSelect){
	foreach($danhMuc as $banGhi){
		if($banGhi['parent']==$idCha){

			if ($banGhi['id']== $idSelect) 
			{
				echo '<option selected value = "'.$banGhi['id'].'">'.$chuoiTab.$banGhi['name'].'</option>';  			
			}else{
				echo '<option value = "'.$banGhi['id'].'">'.$chuoiTab.$banGhi['name'].'</option>';  			

			}
			getCategory($danhMuc, $banGhi['id'], $chuoiTab.'---|');
		}
	}	
}

function showCategory($danhMuc, $idCha, $chuoiTab){
	foreach($danhMuc as $banGhi){
		if($banGhi['parent']==$idCha){
			echo'<div class="item-menu"><span>'.$chuoiTab.$banGhi['name'].'</span>
					<div class="category-fix">
						<a class="btn-category btn-primary" href="/admin/category/edit/'.$banGhi['id'].'"><i class="fa fa-edit"></i></a>
						<a class="btn-category btn-danger" href="/admin/category/del/'.$banGhi['id'].'"><i class="fa fa-close"></i></a>

					</div>
				</div>';  
			showCategory($danhMuc, $banGhi['id'], $chuoiTab.'---|');
		}
	}	
}


function getLevel($danhMuc, $idCha, $cap){
	foreach ($danhMuc as $banGhi) {
		if($banGhi['id']== $idCha){
			$cap++;
			if($banGhi['parent']==0) {
				return $cap;
			}
			return getLevel($danhMuc, $banGhi['parent'], $cap);
		}
		
	}
}




?>