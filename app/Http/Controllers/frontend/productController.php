<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\MOdel\Category;

class productController extends Controller
{
   

    function getShop(Request $r){

    	if($r->start != ""){
    		$data['pro']=Product::whereBetween('price', [$r->start, $r->end])->paginate(1);
    	}else{
    		$data['pro']=Product::orderBy('id', 'DESC')->paginate(3);
    	}
     
        $data['cate']=Category::get();
    	return view('frontend.product.shop', $data);
    }

     function getDetail($str){
        $array = explode('-', $str);
        $id = array_pop($array);
     	$data['product']=Product::find($id);
    	return view('frontend.product.detail', $data);
    	
    }

    
}
