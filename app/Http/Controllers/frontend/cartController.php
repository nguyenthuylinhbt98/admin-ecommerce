<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use Cart;
class cartController extends Controller
{
    function getCart(){
    	$data['cart']=Cart::content();
    	$data['total']=Cart::total(0, '', '.');
    	return view('frontend.cart.cart', $data);
    }

    function addItem(Request $r){
    	// dd($r);
    	$pro = Product::find($r->prdid);
    	Cart::add([
    		'id' => $pro->id, 
    		'name' => $pro->name, 
    		'qty' => $r->quantity, 
    		'price' => $pro->price, 
    		'weight' => 0, 
    		'options' => [
    			'img' => $pro->img, 
    			]
    	]);
    	return redirect('cart');
    }
    function updateItem($rowId, $qty){
    	Cart::update($rowId, $qty);
    	echo "hihi";
    }

    function del($rowId){
    	Cart::remove($rowId);
    	echo "hihi";
    }
}
