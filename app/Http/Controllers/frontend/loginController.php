<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class loginController extends Controller
{
    function getLogin(){
    	return view('backend.login');
    }

    function postLogin(Request $r){
    	$email = $r->email;
    	$password = $r->password;
    	// đăng nhập với trường email = $email, trương pass= $password
    	// đk để dùng Auth: mk phải được mã hóa dưới dang mã bcrypt
    	if(Auth::attempt(['email'=>$email, 'password' => $password])){
    		return redirect('/admin');
    	}else{
    		return redirect()->back()->withErrors(['email'=>'Tài khoản hoặc mật khẩu không chính xác!!'])->withInput();
    	}

    	
    }
}
