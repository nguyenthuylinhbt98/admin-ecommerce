<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use Auth;
use App\Model\{Order, Product_Order, User};
class checkoutController extends Controller
{
    function getCheckout(){
    	$data['cart']=Cart::content();
    	$data['total']=Cart::total(0, '', '.');
    	return view('frontend.checkout.checkout', $data);
    }

    function postCheckout(Request $r){
    	// dd($r->all());
    	// $data['info']=$r->all();
    	// $data['cart']= Cart::content();
    	// $data['total']= Cart::total();

    	$order = new Order();
    	$order->full = $r->full;
    	$order->address = $r->address;
    	$order->email = $r->email;
    	$order->phone = $r->phone;
    	$order->total = Cart::total(0, '', '');
    	$order->state = 2;
    	$order->save();

    	foreach(Cart::content() as $row){
    		$prdOrder = new Product_Order();
    		$prdOrder->code=$row->id;
    		$prdOrder->name=$row->name;
    		$prdOrder->price= $row->price;
    		$prdOrder->quantity=$row->qty;
    		$prdOrder->img=$row->options->img;
    		$prdOrder->order_id=$order->id;
    		$prdOrder->save();
    	}

    	return redirect('/checkout/complete/'.$order->id);

    }

    function getComplete($id){
    	$data['order']=Order::find($id);
    	return view('frontend.checkout.complete', $data);
    }
}
