<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Order;


class orderController extends Controller
{

	function getList(){
		$data['order']=Order::where('state', 2)->paginate(4);
    	return view('backend.order.order', $data);
    }


    function getDetail($id){
    	$data['order']=Order::find($id);
    	return view('backend.order.detailorder', $data);
    }

    function getProcessed(){
		$data['order']=Order::where('state', 1)->paginate(4);
    	return view('backend.order.processed', $data);
    }

    function xu_li($id){
    	// dd(123);
    	$order=Order::find($id);
    	$order->state = 1;
    	$order->save();
    	return redirect('/admin/order')->with('thongbao', 'một đơn hàng vừa được xử lí');

    }
}
