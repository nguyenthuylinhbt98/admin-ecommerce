<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AddUserRequest;
use App\Http\Requests\Backend\EditUserRequest;

use App\User;

class userController extends Controller
{
	function __construct(){
		$this->user = new User();
	}
    function getAdd(Request $r){
		return view('backend.user.adduser');
	}

	function postAdd(AddUserRequest $r){
    	$user = $this->user;
        $user->email =$r->email;
        $user->password =$r->password;
        $user->full =$r->full;
        $user->address =$r->address;
        $user->phone =$r->phone;
        $user->level =$r->level;

        $user->save();
		 return redirect()->back()->with('thongbao', 'đã thêm thành công');
	}


	function getEdit($iduser){
		// dd(123);
		// $data['user'] = User::findOrFail($iduser);
		$user = $this->user->findOrFail($iduser);
		return view('backend.user.edituser', compact('user'));

	}

	function postEdit($iduser, EditUserRequest $r){
		$user = $this->user->findOrFail($iduser);
		$user->email =$r->email;
        $user->password =$r->password;
        $user->full =$r->full;
        $user->address =$r->address;
        $user->phone =$r->phone;
        $user->level =$r->level;

        $user->save();
		return redirect()->back()->with('thongbao', 'đã sửa thành công');

	}


	function getList(){
		// $data['users']= User::all()::paginate(2);
		//return view('backend.user.listuser', $data);
		$users = $this->user->getList();
		return view('backend.user.listuser', compact('users'));

	}


	function delete($iduser){
		// dd(123);
        User::destroy($iduser);

        return redirect()->back()->with('thongbao', 'đã xóa thành công');

    }
}
