<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AddCategoryRequest;
use App\Http\Requests\Backend\EditCategoryRequest;
use App\Model\Category;


class categoryController extends Controller
{


	function getList(){
		$data['categories'] = category::all()->toarray();
		
		return view('backend.category.category', $data);
	}

	function postAdd(AddCategoryRequest $r){
		$categories = category::all()->toarray();
		if( getLevel($categories, $r->idParent, 1)<3){

			$cate = new Category;
			$cate->name=$r->name;
			$cate->slug=str_slug($r->name);
			$cate->parent=$r->idParent;
			$cate->save();
			return redirect()->back()->with('thongbao', 'đã thêm thành công');
		}else{
			return redirect()->back()->withErrors(['name'=>'danh mục vượt quá số cấp mà giao diện hỗ trợ'])->withInput();
			// phải có withInput thì mới tồn tại biến old để lấy giá trị cũ
			// old chỉ tồn tại khi giao diện trả về từ form request, ở trên là trả về từ laravel
		}
		
	}


    function getEdit($id){
		$data['categories'] = category::all()->toarray();
		$data['cate'] = category::findOrFail($id);
		return view('backend.category.editcategory', $data);
	}

	function postEdit($id, EditCategoryRequest $r){

		$categories = category::all()->toarray();
		if( getLevel($categories, $r->idParent, 1)<3){

			$cate = category::findOrFail($id);
			$cate->name=$r->name;
			$cate->slug=str_slug($r->name);
			$cate->parent=$r->idParent;
			$cate->save();
			return redirect()->back()->with('thongbao', 'đã sửa thành công');
		}else{
			return redirect()->back()->withErrors(['name'=>'danh mục vượt quá số cấp mà giao diện hỗ trợ'])->withInput();
			// phải có withInput thì mới tồn tại biến old để lấy giá trị cũ
			// old chỉ tồn tại khi giao diện trả về từ form request, ở trên là trả về từ form
		}
		
	}

	function delCate($id){
		Category::destroy($id);
        return redirect()->back()->with('thongbao', 'đã xóa thành công');
	}



}
