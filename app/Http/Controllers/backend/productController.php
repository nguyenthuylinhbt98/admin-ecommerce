<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AddProductRequest;
use App\Http\Requests\Backend\EditProductRequest;
use App\Model\Product;
use App\Model\Category;
use Auth;
use App\User;

class productController extends Controller
{
	// hien thi danh sách sản phẩm
	function getList(){
		$data['products'] = Product::paginate(4);
		return view('backend.product.listproduct', $data);

	}


	// hien thi add product
	function getAdd(){

		
		// dd(Auth::user());
		if ($user->can('add', User::class)) {

			echo "cho phép thêm";
		}else{
			echo "bạn k có quyền thêm";
		}
		// $data['cate']=Category::all();

		// return view('backend.product.addproduct', $data);
	}

	// xử lí dữ liệu
	function postAdd(AddProductRequest $r){
		$pr = new Product();
		$pr->code=$r->code;
		$pr->name=$r->name;
		$pr->slug=str_slug($r->name);
		$pr->price=$r->price;
		$pr->featured=$r->featured;
		$pr->state=$r->state;
		$pr->info=$r->info;
		$pr->describe=$r->describe;
		if($r->hasFile('img')){
			// đường dẫn tương đối của file
			$file = $r->img;
			// lấy tên file
			$fileName = str_slug($r->name).'.'.$file->getClientOriginalExtension();

			// $file->move('đường dẫn lưu file(đường dẫn cnhinf từ index)', 'tên file sau khi upload'); 
			$file->move('backend/img', $fileName);
			// $nameFile = 'img/'.$fileName;
			$pr->img=$fileName;
		}else{
			$pr->img='no-img.jpg';
		}

		$pr->category_id=$r->category;
		$pr->save();
		return redirect('admin/product')->with('thongbao', 'đã thêm thành công');
	}


	function getEdit($id){
		$user=Auth::user();
		if ($user->can('edit', User::class)) {
			echo "cho phép thêm";
		}else{
			echo "bạn k có quyeend thêm";
		}


		$data['cate']=Category::all();
		$data['product']= Product::find($id);
		return view('backend.product.editproduct', $data);

	}

	function postEdit(EditProductRequest $r, $id){
		$pro = Product::find($id);
		$pro->code=$r->code;
		$pro->name=$r->name;
		$pro->slug=str_slug($r->name);
		$pro->price=$r->price;
		$pro->featured=$r->featured;
		$pro->state=$r->state;
		$pro->info=$r->info;
		$pro->describe=$r->describe;
		if($r->hasFile('img')){
			// ktra xem anh nay đã tồn tại hay chưa
			if(file_exists('backend/img/'.$pro->img)){
				// xóa hình ảnh cũ đi, tránh phình bộ nhớ
				if($pro->img!='no-img.jpg'){
					unlink('backend/img/'.$pro->img);
				}
			}

			// đường dẫn tương đối của file
			$file = $r->img;
			// lấy tên file
			$fileName = str_slug($r->name).'.'.$file->getClientOriginalExtension();
			// $file->move('đường dẫn lưu file(đường dẫn cnhinf từ index)', 'tên file sau khi upload'); 
			$file->move('backend/img', $fileName);
			// $nameFile = 'img/'.$fileName;
			$pro->img=$fileName;
		}
		$pro->category_id=$r->category;
		$pro->save();
		return redirect('admin/product')->with('thongbao', 'đã sửa thành công');
		
	}

	function delete($id){
		dd(123);
		Product::destroy($id);
		return redirect('admin/product')->with('thongbao', 'đã xóa thành công');

	}

	
}
