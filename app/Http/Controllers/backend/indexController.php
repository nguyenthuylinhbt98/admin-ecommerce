<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon; 
use App\Model\Order;
class indexController extends Controller
{
    function getIndex(){

    	$month_now = Carbon::now()->format('m');
    	$year_now = Carbon::now()->format('Y');
    	for($i=1; $i<=$month_now; $i++){
    		$dl["Tháng ".$i]= Order::where('state', 1)
                            ->whereMonth('updated_at', $i)
                            ->whereYear('updated_at', $year_now)
                            ->sum('total');
        	}

    	$data['dl']= $dl;
    	$data['don_hang']= Order::where('state', 2)->count();
    	
    	return view('backend.index', $data);
    }
    function postLogout(){
    	Auth::logout();
    	return  redirect('/login');
    }
}
