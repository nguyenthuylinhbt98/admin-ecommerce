<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;

class ApiController extends Controller
{
    function getProduct(){
    	return product::paginate(3);
    }

    function detailProduct($idPrd){
    	$data['chi_tiet_san_pham']=product::find($idPrd);
    	$data['san_pham_moi']=product::where('featured', 1)->orderBy('id', 'ASC')->take(4)->get();
    	return response()->json($data, 200);
    }

    function sendMail(Request $r){
    	$data['email']=$r->email;
    	$data['thongbao']='thành công';
    	return response()->json($data, 200);
    }

}
