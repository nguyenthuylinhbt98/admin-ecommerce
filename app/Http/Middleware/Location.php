<?php

namespace App\Http\Middleware;

use Closure;

class Location
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('lang')){
            $lang = session('lang');
        }else{
            $lang='vi';
        }
        \App::setLocale($lang);
        return $next($request);

    }
}
