<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     //
    // }


    // tên modle (lấy biến từ Auth)
    function view(User $user){
        return true;
    }

    function add(User $user){
        return $user->level==1||$user->level==2;
    }

    function edit(User $user){
        return $user->level==1;
    }

    function del(User $user){
        return $user->level==1;
    }
}
