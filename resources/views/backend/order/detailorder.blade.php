@extends('backend.master.master')
@section('order', 'class=active')

@section('content')

<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use>
						</svg></a></li>
				<li class="active">Đơn hàng / Chi tiết đặt hàng</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Chi tiết đặt hàng</div>
					<div class="panel-body">

						@if(session('thongbao'))
                                    <div class="alert bg-success" role="alert">
                                        <svg class="glyph stroked checkmark">
                                            <use xlink:href="#stroked-checkmark"></use>
                                        </svg>{{session('thongbao')}}<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                    </div>

                                @endif
						<div class="bootstrap-table">
							<div class="table-responsive">
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<div class="panel panel-blue">
												<div class="panel-heading dark-overlay">Thông tin khách hàng</div>
												<div class="panel-body">
													<strong><span class="glyphicon glyphicon-user" aria-hidden="true"></span> : {{$order->full}}</strong> <br>
													<strong><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> : {{$order->phone}}</strong>
													<br>
													<strong><span class="glyphicon glyphicon-send" aria-hidden="true"></span> : {{$order->address}}</strong>
												</div>
											</div>
										</div>
									</div>


								</div>
								<table class="table table-bordered" style="margin-top:20px;">
									<thead>
										<tr class="bg-primary">
											<th>ID</th>
											<th>Thông tin Sản phẩm</th>
											<th>Giá sản phẩm</th>
											<th>Thành tiền</th>

										</tr>
									</thead>
									<tbody>
										@foreach($order->product_order as $item)
											<tr>
												<td>{{$item->id}}</td>
												<td>
													<div class="row">
														<div class="col-md-4">
															<img width="100px" src="img/{{$item->img}}" class="thumbnail">
														</div>
														<div class="col-md-8">
															<p><b>Mã sản phẩm</b>: {{$item->code}}</p>
															<p><b>Tên Sản phẩm</b>: {{$item->name}}</p>
															<p><b>Số lương</b> : {{$item->quantity}}</p>
														</div>
													</div>
												</td>
												<td>{{number_format($item->price,0, '', '.')}} VNĐ</td>
												<td>{{number_format($item->price*$item->quantity,0, '', '.')}} VNĐ</td>

											</tr>
											

										@endforeach
									
									</tbody>

								</table>
								<table class="table">
									<thead>
										<tr>
											<th width='70%'>
												<h4 align='right'>Tổng Tiền :</h4>
											</th>
											<th>
												<h4 align='right' style="color: brown;">{{number_format($order->total, 0, "", ".")}} VNĐ</h4>
											</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								<div class="alert alert-primary" role="alert" align='right'>
									<a class="btn btn-success" href="#" role="button" data-toggle="modal" data-target="#exampleModal">Đã xử lý</a>
								</div>

								<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							        <div class="modal-dialog" role="document">
							            <div class="modal-content">
							                <div class="modal-header">
							                    <h5 class="modal-title" id="exampleModalLabel">Xác nhận xử lí đơn hàng</h5>
							                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <span aria-hidden="true">&times;</span>
							                    </button>
							                </div>
							                <form action="/admin/order/xu-li/{{$order->id}}" method="POST" accept-charset="utf-8">
							                    <div class="modal-body">
							                        @csrf
							                        <h4>Đơn hàng đã xử lí sẽ được tính vào doanh thu</h4>
							                    </div>
							                    <div class="modal-footer">
							                        <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
							                        <button type="submit" class="btn btn-danger">Xác nhận</button>
							                    </div>
							                </form>
							            </div>
							        </div>
							    </div>    
							                        
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->


	</div>
	<!--end main-->

@endsection
