@extends('backend.master.master')
@section('category', 'class=active')
@section('content')
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use>
						</svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Quản lý danh mục</h1>
			</div>
		</div>
		<!--/.row-->

								@if(session('thongbao'))
                                    <div class="alert bg-success" role="alert">
                                        <svg class="glyph stroked checkmark">
                                            <use xlink:href="#stroked-checkmark"></use>
                                        </svg>{{session('thongbao')}}<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                    </div>

                                @endif
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-5">
								<form action="/admin/category/edit/{{$cate->id}}" method="post" accept-charset="utf-8">
									@csrf

									

									<div class="form-group">
										<label for="">Danh mục cha:</label>
										<select class="form-control" name="idParent" id="">
											<option value="0">----ROOT----</option>
											{{ getCategoryEdit($categories, 0, '', $cate->parent) }}
										</select>
									</div>
									<div class="form-group">
										<label for="">Tên Danh mục</label>
										<input type="text" class="form-control" name="name"  placeholder="Tên danh mục mới" value="{{$cate->name}}">
										{{ ShowErrors($errors, 'name')}}
										
									</div>
									<button type="submit" class="btn btn-primary">Sửa danh mục</button>

									
								</form>

								
							</div>
							<div class="col-md-7">
								<h3 style="margin: 0;"><strong>Phân cấp Menu</strong></h3>
								<div class="vertical-menu">
									<div class="item-menu active">Danh mục </div>
									{{ showCategory($categories, 0, '') }}

													<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							                            <div class="modal-dialog" role="document">
							                                <div class="modal-content">
							                                    <div class="modal-header">
							                                        <h5 class="modal-title" id="exampleModalLabel">Xóa thành viên</h5>
							                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                                        <span aria-hidden="true">&times;</span>
							                                        </button>
							                                    </div>
							                                    <form action="" method="POST" accept-charset="utf-8">
							                                        <div class="modal-body">
							                                            @csrf
							                                            <h4>Bạn có chắc chắn muốn xóa người dùng này</h4>
							                                        </div>
							                                        <div class="modal-footer">
							                                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
							                                            <button type="submit" class="btn btn-danger">Xóa</button>
							                                        </div>
							                                    </form>
							                                </div>
							                            </div>
							                        </div>

								</div>
							</div>
						</div>
					</div>
				</div>



			</div>
			<!--/.col-->


		</div>
		<!--/.row-->
	</div>
	<!--/.main-->
	
@endsection

@section('script')
	<script>
		$('#calendar').datepicker({});

		! function ($) {
			$(document).on("click", "ul.nav li.parent > a > span.icon", function () {
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
			if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
			if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>

@endsection
