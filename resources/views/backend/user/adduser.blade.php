@extends('backend.master.master')
@section('user', 'class=active')

@section('content')
     <!--main-->
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm Thành viên</h1>
            </div>
        </div>
                                @if(session('thongbao'))
                                    <div class="alert bg-success" role="alert">
                                        <svg class="glyph stroked checkmark">
                                            <use xlink:href="#stroked-checkmark"></use>
                                        </svg> {{session('thongbao')}} <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                    </div>

                                @endif
        <!--/.row-->
    <form action="/admin/user/add" method="post" accept-charset="utf-8">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><i class="fas fa-user"></i> Thêm thành viên</div>
                        <div class="panel-body">
                            <div class="row justify-content-center" style="margin-bottom:40px">

                                <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                 
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input value="{{old('email')}}" type="text" name="email" class="form-control">
                                         {{ ShowErrors($errors, 'email')}}
                                    </div>
                                    <div class="form-group">
                                        <label>password</label>
                                        <input value="{{old('password')}}" type="text" name="password" class="form-control">
                                         {{ ShowErrors($errors, 'password')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Full name</label>
                                        <input value="{{old('full')}}" type="full" name="full" class="form-control">
                                         {{ ShowErrors($errors, 'full')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input value="{{old('address')}}" type="address" name="address" class="form-control">
                                         {{ ShowErrors($errors, 'address')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input value="{{old('phone')}}" type="phone" name="phone" class="form-control">
                                         {{ ShowErrors($errors, 'phone')}}
                                    </div>
                                  
                                    <div class="form-group">
                                        <label>Level</label>
                                        <select name="level" class="form-control">
                                            <option value="1">admin</option>
                                            <option selected value="2">user</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">
                                      
                                        <button class="btn btn-success"  type="submit">Thêm thành viên</button>
                                        <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                    </div>
                                </div>
                               

                            </div>
                        
                            <div class="clearfix"></div>
                        </div>
                    </div>

            </div>
        </div>
    </form>

        <!--/.row-->
    </div>

    <!--end main-->

@endsection
