@extends('backend.master.master')
@section('user', 'class=active')

@section('content')

<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use>
						</svg></a></li>
				<li class="active">Danh sách thành viên</li>
			</ol>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Danh sách thành viên</h1>
			</div>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">

					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								@if(session('thongbao'))
									<div class="alert bg-success" role="alert">
										<svg class="glyph stroked checkmark">
											<use xlink:href="#stroked-checkmark"></use>
										</svg>{{session('thongbao')}}<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
									</div>

								@endif
								<a href="/admin/user/add" class="btn btn-primary">Thêm Thành viên</a>
								<table class="table table-bordered" style="margin-top:20px;">

									<thead>
										<tr class="bg-primary">
											<th>ID</th>
											<th>Email</th>
											<th>Full</th>
											<th>Address</th>
                                            <th>Phone</th>
                                            <th>Level</th>
											<th width='18%'>Tùy chọn</th>
										</tr>
									</thead>
									<tbody>
										@foreach($users as $user)
											<tr>
												<td>{{$user->id}}</td>
												<td>{{$user->email}}</td>
												<td>{{$user->full}}</td>
												<td>{{$user->address}}</td>
	                                            <td>{{$user->phone}}</td>
	                                            <td>
	                                            	@if($user->level ==1) Admin @else User @endif

	                                            </td>
												<td>
													<a href="/admin/user/edit/{{$user->id}}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
													<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
													<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							                            <div class="modal-dialog" role="document">
							                                <div class="modal-content">
							                                    <div class="modal-header">
							                                        <h5 class="modal-title" id="exampleModalLabel">Xóa thành viên</h5>
							                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                                        <span aria-hidden="true">&times;</span>
							                                        </button>
							                                    </div>
							                                    <form action="/admin/user/delete/{{$user->id}}" method="POST" accept-charset="utf-8">
							                                        <div class="modal-body">
							                                            @csrf
							                                            <h4>Bạn có chắc chắn muốn xóa người dùng này</h4>
							                                        </div>
							                                        <div class="modal-footer">
							                                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
							                                            <button type="submit" class="btn btn-danger">Xóa</button>
							                                        </div>
							                                    </form>
							                                </div>
							                            </div>
							                        </div>
												</td>
	                                        </tr>

										@endforeach
								
									</tbody>
								</table>
								<div align='right'>
									<ul class="pagination">
										{{-- <li class="page-item"><a class="page-link" href="#">Trở lại</a></li>
										<li class="page-item"><a class="page-link" href="#">1</a></li>
										<li class="page-item"><a class="page-link" href="#">2</a></li>
										<li class="page-item"><a class="page-link" href="#">3</a></li>
										<li class="page-item"><a class="page-link" href="#">tiếp theo</a></li> --}}
										{{$users->links()}}

									</ul>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>
				<!--/.row-->


			</div>
			<!--end main-->

			<!-- javascript -->
			<script src="js/jquery-1.11.1.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script src="js/chart.min.js"></script>
			<script src="js/chart-data.js"></script>
			


@endsection
