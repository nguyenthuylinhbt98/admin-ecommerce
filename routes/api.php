<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// api gồm 4 phuong thức
// get lấy dữ liệu
// post thêm dữ liệu
// put sửa dữ liệu
// delete xóa dữ liệu

Route::get('get-product', 'Api\ApiController@getProduct');
Route::get('detail/{idPrd}/product', 'Api\ApiController@detailProduct');

Route::post('send-mail', 'Api\ApiController@sendMail');
