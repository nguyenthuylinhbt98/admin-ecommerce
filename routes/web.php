<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*----------------------------------------lythuyet----------------------------*/
route::group(['prefix'=>'thanhvien'],function()
{
    route::get('/add', function(){
        echo 'trang add';
    });

    route::get('/edit', function(){
         echo 'trang edit';
    });
    route::get('/del', function(){
        echo 'xoa';
    });

});

// schema các mã lệnh tương tác với bảng
Route::group(['prefix' => 'schema'], function() {
    Route::get('create-table', function() {
        
        Schema::create('bang1', function ($table) {
            $table->increments('id');
            $table->string('name',100)->default('hihihihi');
            $table->string('address')->nullable();
        }); 


    });
    Route::get('rename-table', function() {
        
        Schema::rename('bang1', 'thongtin');
        echo "thanhcong";       
    });

    Route::get('drop-table', function() {
        // Schema::drop('thongtin');
        // neu co bang thi xoa, k co bang thi bao loi


        Schema::dropIfExists('thongtin');
        // luon chay, neu ton tai bang thi xoa, neu k ton tai thi k co j xay ra
        echo "đã xóa bảng";
              
    });
});



// query buider: các phương thức thực thi truy vấn CSDL
Route::group(['prefix' => 'query'], function() {
    // them ban ghi vao bang
    Route::get('/insert', function() {
        // them 1 ban ghi
        // DB::table('users')->insert(['email'=>'demo@gmail.com',
        //                             'password'=>'123',
        //                             'full'=>'Nguyen Linh',
        //                             'address'=>'Bach Khoa',
        //                             'phone'=>'01234',
        //                             'level'=>1]);
        

        // them nhieu ban ghi
        DB::table('users')->insert([
            ['email'=>'demo0@gmail.com','password'=>'123','full'=>'Nguyen Linh','address'=>'Bach Khoa','phone'=>'01234','level'=>1],
            ['email'=>'demo2@gmail.com','password'=>'123','full'=>'Thuy Linh','address'=>'Bach Khoa','phone'=>'012345','level'=>2],
            ['email'=>'demo3@gmail.com','password'=>'123','full'=>'Linh','address'=>'Bach Khoa','phone'=>'0123456','level'=>2],
            ['email'=>'demo4@gmail.com','password'=>'123','full'=>'Nguyen Thuy Linh','address'=>'Bach Khoa','phone'=>'01234567','level'=>1]



        ]);


     });

    // cap nhat
    Route::get('/update', function() {
        // DB::table('users')->where('id', '>', '3')->update(['password'=>'123456']);

        // khong co tham so t2 (where) tu dong hieu la bang
        DB::table('users')->where('id', '3')->update(['password'=>'456']);

    });

    // xoa ban ghi trong bang
    Route::get('delete', function() {
        // DB::table('users')->where('id','>', '3')->delete();
        DB::table('users')->delete();

        
    });

    // lenh truy van nang cao
    // chu y: tat ca cau lenh lay du lieu deu dung get() hoac first() khi ket thuc


    //get (lay toan bo du lieu trong bang)
    Route::get('get', function() {
        $user = DB::table('users')->get()->toarray();
        dd($user[1]->full);
    });


    // first : lay ban ghi dau tien trong bang
    Route::get('first', function() {
        $user = DB::table('users')->first();
        dd($user->phone);
    });

    // where
    Route::get('where', function() {
        $user = DB::table('users')->where('full', 'like', '%the%')->get();
        dd($user);
    });

    // where and
    Route::get('where-and', function() {
        $user = DB::table('users')->where('full', 'like', '%the%')->where('level', 2)->get();
        dd($user);
    });


    // where or
    Route::get('where-or', function() {
        $user = DB::table('users')->where('full', 'like', '%the%')->orwhere('level', 2)->get();
        dd($user);
    });

    // where in (tim nhung ban ghi voi cot co du lieu o trong mang)
    Route::get('where-in', function() {
        $user = DB::table('users')->wherein('id',[1, 3, 4])->get();
        dd($user);
    });

    // take (lay ra bao nhieu ban ghi)
    Route::get('take', function() {
        $user = DB::table('users')->take(2)->get();
        // lay ra 2 ban ghi dau, bo qua tat ca ban ghi phia sau

        dd($user);
    });

    // skip (thuong di voi take (dung sau take) ->dung tu vi tri nao, lay bao nhieu phan tu)
    Route::get('skip', function() {
        $user = DB::table('users')->take(2)->skip(2)->get();
        //  bo qua 2 ban ghi đầu, lấy sau đó 2 bản ghi tiep theo

        dd($user);
    });


    // CAU LENH THUC THI
    // increment (tang)
    Route::get('increment', function() {
        // $user = DB::table('users')->increment('level', 4);
        // tang tat ca cac cot level trong bang users len 4 don vi

        $user = DB::table('users')->where('level', '>', 3)->increment('level', 4);
    });

    // decrement (nguoc lai voi increment. giam)

});


// pivot -- chỉ tương tác với bảng trung gian
// them
Route::get('attach', function() {
    //admin@gmail.com(1) đăng kí 3 lớp toan(1), li(2), hoa(3)
    App\Model\User::find(1)->lop()->attach([1,2,3]);

});


// cập nhật
Route::get('Sync', function() {
    App\Model\User::find(1)->lop()->Sync([1,2]);
    
});

// xóa
Route::get('detach', function() {
    // App\Model\User::find(1)->lop()->detach([1]);

    // xóa tất cả
    App\Model\User::find(1)->lop()->detach();

    
});


/*--------------------------------------project---------------------------------------------*/




/*------------------Frontend*------------------*/
Route::group(['namespace'=>'frontend'],function()
{
    Route::get('/', 'indexController@getIndex' );
    Route::get('/about','indexController@getAbout' );
    Route::get('/contact', 'indexController@getContact');
    Route::post('/dang-ki', 'indexController@getDangKi');

    Route::group(['prefix'=>'cart'],function()
    {
        Route::get('/', 'cartController@getCart');
        Route::get('add-item', 'cartController@addItem');
        Route::get('update-item/{rowId}/{qty}', 'cartController@updateItem');
        Route::get('update-item/{rowId}', 'cartController@del');
    });

    Route::group(['prefix'=>'checkout'],function()
    {
        Route::get('/', 'checkoutController@getCheckout');
        Route::post('/', 'checkoutController@postCheckout');
        Route::get('/complete/{id}','checkoutController@getComplete' );
    });

    Route::group(['prefix'=>'product'], function()
    {
        Route::get('/detail/{str}.html', 'productController@getDetail');
        Route::get('/', 'productController@getShop');
    }); 
    Route::get('/login', 'loginController@getLogin');
    Route::post('/login', 'loginController@postlogin');

});




/*--------------------------Admin-------------------------------------------------------*/


