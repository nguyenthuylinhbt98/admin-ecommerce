<?php 

    Route::get('/', 'indexController@getIndex');
    Route::get('/logout', 'indexController@postLogout');


    /*------product-----*/
    Route::group(['prefix'=>'product'],function()
    {
        Route::get('/','productController@getList');

        Route::get('/add', 'productController@getAdd');
        Route::post('/add', 'productController@postAdd');

        Route::get('/edit/{id}', 'productController@getEdit');
        Route::post('/edit/{id}', 'productController@postEdit');

        Route::post('/delete/{id}', 'productController@delete');


    });

    /*--------------user-------------*/

    Route::group(['prefix'=>'user'], function()
    {
        Route::get('/', 'userController@getList');
        
        Route::get('/add', 'userController@getAdd');
        Route::post('/add', 'userController@postAdd');

        Route::get('/edit/{id}', 'userController@getEdit');
        Route::post('/edit/{id}', 'userController@postEdit');

        Route::post('/delete/{iduser}', 'userController@delete');
        
    });

    /*--------category-------------*/
    Route::group(['prefix'=>'category'], function()
    {
        Route::get('/','categoryController@getList');
        Route::post('/','categoryController@postAdd');
        Route::get('/edit/{id}','categoryController@getEdit');
        Route::post('/edit/{id}','categoryController@postEdit');
        Route::post('/delete/{id}', 'categoryController@delCate');

    });

    


    /*---------------order--------------*/
    Route::group(['prefix'=>'order'],function()
    {
        Route::get('/','orderController@getList');

        Route::get('/detail/{id}', 'orderController@getDetail');
        Route::post('/detail/{id}', 'orderController@postDetail');

        Route::post('/xu-li/{id}', 'orderController@xu_li');

        Route::get('/processed','orderController@getProcessed');
    });

    


 ?>